/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hdfs.finalproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.lang.Math;
import org.apache.hadoop.fs.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.HBaseConfiguration;

import hdfs.finalproject.HBase.*;


/**
 *
 * @author lovelioula
 */
public class getLotteryNum {   
    
	private static String Six_Lottery_Table = "Six_Lottery";
	private static String Big_Lottery_Table = "Big_Lottery";
	private static String Power_Lottery_Table = "Power_Lottery";

	public static void main(String[] args) throws IOException         
	{      
		//Configuration configuration = createHbaseConf();
		//AggregationClient aggregationClient = new AggregationClient(configuration);		
	try {
		HBase.createHBaseTable(Six_Lottery_Table,Six_Lottery_Table);
		HBase.createHBaseTable(Big_Lottery_Table,Big_Lottery_Table);
		HBase.createHBaseTable(Power_Lottery_Table,Power_Lottery_Table);
	} catch (IOException ex) {
		System.out.println("<createHBaseTable exception> " + ex.toString());
	}
        
		String six_url = "http://www.pilio.idv.tw/ltohk/list.asp"; //六合彩
		String big_url = "http://www.pilio.idv.tw/ltobig/list.asp"; //大樂透
		String power_url = "http://www.pilio.idv.tw/lto/list.asp"; //威力採

		//mycrawler(six_url,1);
		mycrawler(big_url,2);
		//mycrawler(power_url,3);
	}
    
	private static void mycrawler(String url,int type) throws MalformedURLException, IOException
	{
		URL myurl = new URL(url);
		StringBuilder builder = new StringBuilder();
		try (BufferedReader in = new BufferedReader(new InputStreamReader(myurl.openStream()))) {
			String inputline = "";
			while ((inputline = in.readLine()) != null) builder.append(inputline);
        }
        catch (IOException e) {
           System.out.println("<Exception.> " + e.toString());
       }
        //  System.out.println(builder.toString()); 
        // String ss = new String(builder.toString().getBytes("big5"),"UTF-8");
        //  System.out.println(ss); 
         Pattern pattern = Pattern.compile("<a href=[\"]?list.asp\\?indexpage=");  //算共有幾頁
         Matcher  matcher = pattern.matcher(builder.toString()); 
         int count = 0;
         while (matcher.find())count++;
        // System.out.println("count ="  + Integer.toString(count));  

        for(int i=1;i<count+1;++i)get_each_page(url+"?indexpage="+Integer.toString(i),type);
        //get_each_page(url+"?indexpage=1",type);
        
    }
    
     private static void get_each_page(String url,int type) throws MalformedURLException, UnsupportedEncodingException, IOException //取得每一頁表格資料~
    {
		URL myurl = new URL(url);
		StringBuilder builder = new StringBuilder();
	try (BufferedReader in = new BufferedReader(new InputStreamReader(myurl.openStream()))) {
			String inputline = "";
			while ((inputline = in.readLine()) != null) builder.append(inputline);
		}
		catch (IOException e) {
			System.out.println("<Exception.> " + e.toString());
		}
		//String content = new String(builder.toString().getBytes("big5"),"UTF-8");
		//System.out.println(content);
		// String word = "期數";

		int start_position = builder.toString().lastIndexOf("<td width=\"23%\" bgcolor=\"#FFEBD7\" align=\"center\"><b><font color=\"#000000\"");  //找到表格起始
		int end_position = builder.toString().lastIndexOf("</table>");  //找到表格結束
		String table_content = builder.toString().substring(start_position, end_position);

		start_position = table_content.indexOf("<tr>");
		table_content = table_content.substring(start_position);
		// System.out.println(table_content);
        
		int spos= start_position,no=1;  //找第1 3 4個
		while(spos != -1)
		{
			spos = table_content.indexOf("<b>")+3;  
			int epos = table_content.indexOf("</b>");
			String id = table_content.substring(spos, epos);
			table_content = table_content.substring(epos+4);

			spos = table_content.indexOf("<b>")+3;  
			epos = table_content.indexOf("</b>");
			String date = table_content.substring(spos, epos);
			table_content = table_content.substring(epos+4);

			spos = table_content.indexOf("<b>")+3;  
			epos = table_content.indexOf("</b>");
			String prize_num = table_content.substring(spos, epos).replaceAll("&nbsp;&nbsp;", ",");
			prize_num = prize_num.replaceAll("&nbsp;", "");
			table_content = table_content.substring(epos+4);		
            
			spos = table_content.indexOf("<b>")+3;  
			epos = table_content.indexOf("</b>");
			String special_num = table_content.substring(spos, epos);
			table_content = table_content.substring(epos+4);
            
			/* odd-even offset */
			String[] numArr = prize_num.split(",");
			int[] num = new int[numArr.length];
			int Numsum,OEnum,i,odd,even;
			Numsum = OEnum = i = odd = even = 0;
			for (String n : numArr){
				num[i] = Integer.parseInt(n);
				if(num[i]%2==1)	odd++;
				else			even++;
				Numsum += num[i];
				i++;
			}
			if(Integer.parseInt(special_num)%2==1)	odd++;
			else			even++;
			OEnum = Math.abs(odd-even);
			/* --------------- */
      
			/*把資料存入HBASE*/
			String TBN_family="";
			if(type==1)TBN_family="Six_Lottery";
			else if (type==2)TBN_family="Big_Lottery";
			else if (type==3)TBN_family="Power_Lottery";

			//HBase.putData(TBN_family,id,TBN_family,"date",date);  //日期
			//HBase.putData(TBN_family,id,TBN_family,"prize_num",prize_num);  //號碼
			//HBase.putData(TBN_family,id,TBN_family,"special_num",special_num); //特別號
			HBase.putData(TBN_family,id,TBN_family,"odd-even_count",Integer.toString(OEnum)); //奇偶差值
			HBase.putData(TBN_family,id,TBN_family,"num_sum",Integer.toString(Numsum)); //選號相加總和
			
			System.out.println("[table] " + TBN_family);
			System.out.println("row_id=" + id);
			System.out.println("date=" + date);
			System.out.println("prize_num=" + prize_num);
			System.out.println("special_num=" + special_num);
			for(int n : num){
				System.out.println("=> integer num: "+n);
			}
			System.out.println("odd-even_num=" + OEnum);
			System.out.println("num_sum=" + Numsum);
			System.out.println("---------------------------");
			/*---------*/

			spos = table_content.indexOf("<tr>");  
			if(spos!=-1)table_content = table_content.substring(spos);
		}
	}
	/*
	//---HBASE---
	private static Configuration createHbaseConf()
	{
		Configuration conf = HBaseConfiguration.create();
		conf.addResource(new Path("/home/course/hbase/conf/hbase-site.xml"));
		return conf;
	}

	public static void createHBaseTable(String tablename) throws IOException
	{
		Configuration conf = createHbaseConf();
		try (Connection connection = ConnectionFactory.createConnection(conf)){
			try(Admin admin = connection.getAdmin()){
							
				HTableDescriptor tableDesc 
					= new HTableDescriptor(TableName.valueOf(tablename));
				HColumnDescriptor column = null;
				
				// if(type==1)column = new HColumnDescriptor(Bytes.toBytes("Six_Lottery"));	
				// else if(type==2)column = new HColumnDescriptor(Bytes.toBytes("Big_Lottery"));	
				// else if(type==3)column = new HColumnDescriptor(Bytes.toBytes("Power_Lottery"));	
				column = new HColumnDescriptor(Bytes.toBytes(tablename));	
				
				tableDesc.addFamily(column);
								
				if (admin.tableExists(TableName.valueOf(tablename))){
					System.out.println("<Table: " + tablename + "Existed.>");
				}else{
					admin.createTable(tableDesc);
					//System.out.println(tablename + " created.");
				}
			}catch(Exception e){}
		}catch(Exception e){}
	}
	//putData(tablename,id,family,"prize_num",prize_num)
	public static void putData(String tablename,String row,String family,String column,String value) 
		throws IOException{
		Configuration conf = createHbaseConf();
		//conf.addResource(new Path("/home/course/hbase/conf/hbase-site.xml"));
		try(Connection conn = ConnectionFactory.createConnection(conf)){
			try(Table table = conn.getTable(TableName.valueOf(tablename))){
				byte[] brow = Bytes.toBytes(row);
				byte[] bfamily = Bytes.toBytes(family);
				byte[] bcolumn = Bytes.toBytes(column);
				byte[] bvalue = Bytes.toBytes(value);

				Put p = new Put(brow);
				p.add(bfamily, bcolumn, bvalue);
				table.put(p);
				table.close();
			}catch(Exception e){}
		}catch(Exception e){}
	}
	*/
}