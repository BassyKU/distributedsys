/**
 *
 *@Author:Bassy
 */
package hdfs.finalproject;

import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.HBaseConfiguration;

public class HBase{
	private static Configuration createHbaseConf()
	{
		Configuration conf = HBaseConfiguration.create();
		conf.addResource(new Path("/home/course/hbase/conf/hbase-site.xml"));
		return conf;
	}

	public static void createHBaseTable(String tablename,String columnname) throws IOException
	{
		Configuration conf = createHbaseConf();
		try (Connection connection = ConnectionFactory.createConnection(conf)){
			try(Admin admin = connection.getAdmin()){
							
				HTableDescriptor tableDesc 
					= new HTableDescriptor(TableName.valueOf(tablename));
				//HColumnDescriptor column = null;
				//column = new HColumnDescriptor(Bytes.toBytes(tablename));	
				HColumnDescriptor column = new HColumnDescriptor(Bytes.toBytes(columnname));	
				tableDesc.addFamily(column);
								
				if (admin.tableExists(TableName.valueOf(tablename))){
					System.out.println("<Table: " + tablename + "Existed.>");
				}else{
					admin.createTable(tableDesc);
					//System.out.println(tablename + " created.");
				}
			}catch(Exception e){}
		}catch(Exception e){}
	}
	//putData(tablename,id,family,"prize_num",prize_num)
	public static void putData(String tablename,String row,String family,String column,String value) 
		throws IOException{
		Configuration conf = createHbaseConf();
		try(Connection conn = ConnectionFactory.createConnection(conf)){
			try(Table table = conn.getTable(TableName.valueOf(tablename))){
				byte[] brow = Bytes.toBytes(row);
				byte[] bfamily = Bytes.toBytes(family);
				byte[] bcolumn = Bytes.toBytes(column);
				byte[] bvalue = Bytes.toBytes(value);

				Put p = new Put(brow);
				p.add(bfamily, bcolumn, bvalue);
				table.put(p);
				table.close();
			}catch(Exception e){}
		}catch(Exception e){}
	}
	public static String getRowData(String tablename,String row,String family,String column) throws IOException{
		byte[] bvalue = null;
		Configuration conf = createHbaseConf();
		try(Connection conn = ConnectionFactory.createConnection(conf)){
			try(Table table = conn.getTable(TableName.valueOf(tablename))){
				byte[] brow = Bytes.toBytes(row);
				byte[] bfamily = Bytes.toBytes(family);
				byte[] bcolumn = Bytes.toBytes(column);
				
				Get g = new Get(brow);
				Result r = table.get(g);
				bvalue = r.getValue(bfamily,bcolumn);
				table.close();
			}catch(Exception e){}
		}catch(Exception e){}
		
		return Bytes.toString(bvalue);
	}
	public static String[] getAllRow(String tablename) throws IOException{
		String rowName = "";
		String[] rowArr = null;
		Configuration conf = createHbaseConf();
		try(Connection conn = ConnectionFactory.createConnection(conf)){
			try(Table table = conn.getTable(TableName.valueOf(tablename))){
				Scan scan = new Scan();
				try(ResultScanner scanner = table.getScanner(scan)){
				    for(Result result : scanner){
						rowName += Bytes.toString(result.getRow());
						rowName += '\n';
					}
					rowArr = rowName.split("\n");
				}catch(Exception e){}
			}catch(Exception e){}
		}catch(Exception e){}
		
		return rowArr;
	}
}