/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hdfs.finalproject;

import java.net.*;
import java.io.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import hdfs.finalproject.Number;
import hdfs.finalproject.Period;
import hdfs.finalproject.HBase;

public class Search {
	private static String Six_Lottery_Table = "Six_Lottery";
	private static String Big_Lottery_Table = "Big_Lottery";
	private static String Power_Lottery_Table = "Power_Lottery";
	
	public static void main(String[] args) throws InterruptedException, MalformedURLException, IOException {
		if (args.length < 2) {
			System.out.println("=============================Search Description=============================");
			System.out.println("argument(1) Lottery type:");
			System.out.println("  [B]ig_Lottery");
			System.out.println("  [S]ix_Lottery");
			System.out.println("  [P]ower_Lottery");
			System.out.println("argument(2) Choose items:");
			System.out.println("  [1]Show the total count of every prize numbers.");
			System.out.println("  [2]Show popular areas which exists first prize.");
			System.out.println("  [3]Input numbers you want to buy, and predict the winning probability:");
			System.out.println("     =>argument(3) \"prize_numbers\" \"special_number\"");
			System.out.println("Example(1): $ hadoop jar finalproject-1.0.jar B 1");
			System.out.println("Example(2): $ hadoop jar finalproject-1.0.jar P 2");
			System.out.println("Example(3): $ hadoop jar finalproject-1.0.jar B 3 \"30 01 05 16 27 26\" \"42\"");
			System.exit(0);
		}
		String tablename = null;
		String[] choose_priNum_str;
		int choose_speNum;
		switch(args[1]){
			case "B": default:
				tablename = Big_Lottery_Table;	break;
			case "S":
				tablename = Six_Lottery_Table;	break;
			case "P":
				tablename = Power_Lottery_Table;	break;				
		}
		/*
		switch(args[2]){
			case "1":
				break;
			case "2":
				break;
			case "3":
				choose_priNum_str = args[3].split(" ");
				choose_speNum = Integer.parseInt(args[4]);
				break;
			default:
		}
		*/
		String[] rowName = HBase.getAllRow(tablename);
		for (String row : rowName){
			String value = HBase.getRowData(tablename,row,tablename,"prize_num");
			value += HBase.getRowData(tablename,row,tablename,"special_num");
			String[] num = value.split(",");
			
			for (String n:num){
				System.out.println(n);
			}			
		}
		/*
		Configuration conf = HBaseConfiguration.create();
		conf.addResource(new Path("/home/course/hbase/conf/hbase-site.xml"));
		try(Connection conn = ConnectionFactory.createConnection(conf)){
			try(Table table = conn.getTable(TableName.valueOf(tablename))){
				Scan scan = new Scan();
				try(ResultScanner scanner = table.getScanner(scan)){
                    Number[] num = new Number[49];
					Period[] periodAnalysis = new Period[2500];
					for (int i=0;i<49;i++)
						num[i] = new Number();
				    for(Result result : scanner){
						String rowName = Bytes.toString(result.getRow());
						int row = Integer.parseInt(rowName);

						for(Cell cell : result.rawCells()){
							String value = Bytes.toString(cell.getValueArray(),
											 cell.getValueOffset(),
											 cell.getValueLength());
							//System.out.println(value);
							String qualifier = new String(CellUtil.cloneQualifier(cell));
							System.out.println("Row = "+rowName+" Qualifier = "+qualifier+" Value = "+value);
							if (qualifier.equals("prize_num")){
								String[] splitArray = value.split(",");
								for (String s : splitArray){
									int content = Integer.parseInt(s);
									//System.out.println(content);
									num[content-1].setPeriod(row,0);
									if (content%2 == 0){
										periodAnalysis[row-1].setEvenNum();
									}
								}
							}
							else if (qualifier.equals("special_num")){
								int content = Integer.parseInt(value);
								//System.out.println(content);
								num[content-1].setPeriod(row,1);
							}
							
						}
				    }
				    for (int i=0;i<49;i++){
						System.out.printf("%d %d",i+1,num[i].allShowTime);
						for (int j=0;j<num[i].periodNum;j++)
							System.out.printf(" %d,",num[i].period[j]);
						System.out.printf("\n");
					}
				}
				
			} catch(Exception e){}
		} catch(Exception e){}
		*/
	} 
/*
	public static void CreateTable() throws IOException
	{
		Configuration conf = create();
		try (Connection connection = ConnectionFactory.createConnection(conf)){
			try(Admin admin = connection.getAdmin()){
				HTableDescriptor tableDesc 
					= new HTableDescriptor(TableName.valueOf("Search_Result"));

				HColumnDescriptor column = new HColumnDescriptor(Bytes.toBytes("column"));				
				tableDesc.addFamily(column);


				admin.createTable(tableDesc);				
			}
		} catch(Exception e){}
	}

	private static Configuration create() {
		Configuration conf = HBaseConfiguration.create();
		conf.addResource(new Path("/home/course/hbase/conf/hbase-site.xml"));
		return conf;
	}
	*/
}